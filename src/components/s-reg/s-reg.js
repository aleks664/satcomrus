const arStep = document.querySelectorAll('.s-reg__step')
function stepValidate(step) {
	const inputsValidate = step.querySelectorAll('[required]')
	inputsValidate.forEach($input => {
		$input.addEventListener('invalid', (e) => {
			e.preventDefault()
			$input.classList.add('is-invalid')
		})
		$input.addEventListener('input', () => {
			$input.classList.remove('is-invalid')
			if ($input.getAttribute('type') === 'radio') {
				const radios = $input.closest('form').querySelectorAll(`input[name="${$input.getAttribute('name')}"]`)
				radios.forEach($radio => {
					$radio.classList.remove('is-invalid')
				})
			}
		})
	})
}

arStep.forEach(step => {
	let next = step.querySelector('.js-next')
	let prev = step.querySelector('.js-prev')
	if (next) {
		next.addEventListener('click', e => {
			stepValidate(step)
			setTimeout(() => {
				if (!step.querySelectorAll('.is-invalid').length) {
					next.closest('.s-reg__step').classList.remove('is-visible')
					if (next.closest('.s-reg__step').nextElementSibling) {
						next.closest('.s-reg__step').nextElementSibling.classList.add('is-visible')
					}
				}
			}, 300)
		})
	}
	if (prev) {
		prev.addEventListener('click', e => {
			e.preventDefault()
			prev.closest('.s-reg__step').classList.remove('is-visible')
			prev.closest('.s-reg__step').previousElementSibling.classList.add('is-visible')
		})
	}
})
