const arrToggleMenu = document.querySelectorAll('.js-toggle-menu')
const navBar = document.getElementById('navbar-mobile')
arrToggleMenu.forEach(btn => {
	btn.addEventListener('click', () => {
		btn.classList.toggle('is-active')
		navBar.classList.toggle('is-open')
		document.documentElement.classList.toggle('is-menu-open')
	})
})
const search = document.getElementById('search')
const arrToggleSearch = document.querySelectorAll('.js-search')
arrToggleSearch.forEach(btn => {
	const input = search.querySelector('input')
	btn.addEventListener('click', e => {
		e.preventDefault()
		if (input.value.length === 0) {
			btn.classList.toggle('is-active')
			search.classList.toggle('is-open')
		} else {
			search.submit()
		}
	})
})
const zoom = document.querySelector('.js-zoom')
zoom.addEventListener('click', e => {
	e.preventDefault()
	zoom.classList.toggle('is-active')
	document.documentElement.classList.toggle('is-zoom')
})
