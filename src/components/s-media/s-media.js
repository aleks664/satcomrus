import Swiper, {
	Navigation
} from 'swiper'
const arrMedia = document.querySelectorAll('.s-media')
arrMedia.forEach(media => {
	let slider = media.querySelector('.swiper')
	let prev = media.querySelector('.s-media__swiper-btn_prev')
	let next = media.querySelector('.s-media__swiper-btn_next')
	const swiper = new Swiper(slider, {
		modules: [Navigation],
		centeredSlides: true,
		slidesPerView: 1,
		spaceBetween: 40,
		loop: true,
		navigation: {
			nextEl: next,
			prevEl: prev
		},
		breakpoints: {
			320: {
				slidesPerView: 1,
				spaceBetween: 40
			},
			768: {
				slidesPerView: 1,
				spaceBetween: 88
			},
			992: {
				slidesPerView: 1,
				spaceBetween: 118
			}
		}
	})
})
import { Fancybox } from '@fancyapps/ui'

const optionsGallery = {
	on: {
		done: (fancybox) => {
			fancybox.container.classList.add('fancybox-custom-gallery')
		}
	},
	Toolbar: {
		enabled: true,
		display: {
			left: [],
			middle: [],
			right: ['zoomIn', 'close']
		}
	},
	Thumbs: {
		type: 'classic'
	}
}
Fancybox.bind('[data-fancybox="gallery"]', optionsGallery)

